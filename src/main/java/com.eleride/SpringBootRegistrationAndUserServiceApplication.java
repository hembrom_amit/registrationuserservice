package com.eleride;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRegistrationAndUserServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRegistrationAndUserServiceApplication.class, args);
	}

}
