package com.eleride.controller;

import com.eleride.common.ErrorMessages;
import com.eleride.common.ServiceException;
import com.eleride.monitor.MonitorApp;
import com.eleride.request.AddressVO;
import com.eleride.request.PaymentTransactionVO;
import com.eleride.request.RegistrationRequest;
import com.eleride.responses.RegistrationResponse;
import com.eleride.service.AddressService;
import com.eleride.service.PaymentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.PathParam;

@Slf4j
@RestController
@Tag(name = "Payment Process", description = "Payment Process")
@CrossOrigin
public class PaymentController {


    @Autowired
    PaymentService paymentService;



    /**
     * Initiate a payment for a user
     *
     * @param paymentTransactionVO
     * @return ResponseEntity
     */
    @Operation(summary = "Initiating a payment transaction for a user", description = "Payment Initiation Process")
    @PostMapping("/paymentInitiation")
    @MonitorApp
    public ResponseEntity<Object> paymentInitiation(@RequestBody PaymentTransactionVO paymentTransactionVO)  {
        try {
            return new ResponseEntity<>(paymentService.paymentInitiation(paymentTransactionVO), HttpStatus.OK);
        } catch (ServiceException se) {
            return ErrorMessages.generateError(se.getError());
        } catch (Exception e) {
            log.error("Exception "+e.getMessage());
            return ErrorMessages.generateError(e);
        }
    }

    /**
     * Update a payment for a user
     *
     * @param paymentTransactionVO
     * @return ResponseEntity
     */
    @Operation(summary = "Update payment transaction for a user", description = "Payment Update Process")
    @PostMapping("/paymentUpdate")
    @MonitorApp
    public ResponseEntity<Object> paymentUpdate(@RequestBody PaymentTransactionVO paymentTransactionVO)  {
        try {
            return new ResponseEntity<>(paymentService.paymentUpdate(paymentTransactionVO), HttpStatus.OK);
        } catch (ServiceException se) {
            return ErrorMessages.generateError(se.getError());
        } catch (Exception e) {
            log.error("Exception "+e.getMessage());
            return ErrorMessages.generateError(e);
        }
    }

}
