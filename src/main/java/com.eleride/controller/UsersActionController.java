package com.eleride.controller;

import com.eleride.common.ErrorMessages;
import com.eleride.common.ServiceException;
import com.eleride.model.Users;
import com.eleride.monitor.MonitorApp;
import com.eleride.request.*;
import com.eleride.responses.LoginResponse;
import com.eleride.service.LoginService;
import com.eleride.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Slf4j
@RestController
@Tag(name = "User Action", description = "User Action")
@CrossOrigin
public class UsersActionController {
    @Autowired
    LoginService loginService;

    @Autowired
    UserService userService;

    /**
     * Get all the employees
     *
     * @return ResponseEntity
     */
    @Operation(summary = "Get all userss", description = "Get all users")
    @GetMapping("/getAllUsers")
    @MonitorApp
    public ResponseEntity<Object> getUsers() {
        try {
            return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
        }  catch (ServiceException se) {
            return ErrorMessages.generateError(se.getError());
        }catch (Exception e) {
            return ErrorMessages.generateError(e);
        }
    }


    /**
     * Create new users
     *
     * @param login
     * @return ResponseEntity
     */
    @Operation(summary = "Login user", description = "Login process")
    @PostMapping("/login")
    @MonitorApp
    public ResponseEntity<Object> login(@RequestBody LoginRequest login) throws ServiceException {
        try {
            LoginResponse loginResponse = loginService.login(login);
            return new ResponseEntity<>(loginResponse, HttpStatus.OK);
        } catch (ServiceException se) {
            return ErrorMessages.generateError(se.getError());
        }catch (Exception e) {
            return ErrorMessages.generateError(e);
        }
    }


    /**
     * Create new users
     *
     * @param login
     * @return ResponseEntity
     */
    @Operation(summary = "Login social user", description = "Login social user process")
    @PostMapping("/sociallogin")
    @MonitorApp
    public ResponseEntity<Object> sociallogin(@RequestBody LoginRequest login) throws ServiceException {
        try {
            LoginResponse loginResponse = loginService.socialLogin(login);
            return new ResponseEntity<>(loginResponse, HttpStatus.OK);
        } catch (ServiceException se) {
            return ErrorMessages.generateError(se.getError());
        }catch (Exception e) {
            return ErrorMessages.generateError(e);
        }
    }


    /**
     * Create new users
     *
     * @param otpRequest
     * @return ResponseEntity
     */
    @Operation(summary = "Verify OTP", description = "OTP verification process")
    @PostMapping("/verifyotp")
    @MonitorApp
    public ResponseEntity<Object> login(@RequestBody OTPRequest otpRequest) throws ServiceException {
        try {
            List<Users> loginResponse = userService.verifyOTP(otpRequest);
            return new ResponseEntity<>(loginResponse, HttpStatus.OK);
        } catch (ServiceException se) {
            return ErrorMessages.generateError(se.getError());
        }catch (Exception e) {
            return ErrorMessages.generateError(e);
        }
    }

    /**
     * Create new users
     *
     * @param forgotPassword
     * @return ResponseEntity
     */
    @Operation(summary = "Forgot Password", description = "Forgot Password")
    @PostMapping("/forgotpassword")
    @MonitorApp
    public ResponseEntity<Object> fotgotpassword(@RequestBody ForgotPassword forgotPassword) throws ServiceException {
        try {
            Users user = userService.forgotPassword(forgotPassword);
            return new ResponseEntity<>(user, HttpStatus.OK);
        } catch (ServiceException se) {
            return ErrorMessages.generateError(se.getError());
        }catch (Exception e) {
            return ErrorMessages.generateError(e);
        }
    }

    /**
     * Create new users
     *
     * @param changePassword
     * @return ResponseEntity
     */
    @Operation(summary = "Verify OTP and change password", description = "Verify OTP and change password")
    @PostMapping("/verifyandchangepassword")
    @MonitorApp
    public ResponseEntity<Object> verifyandchangepassword(@RequestBody ChangePassword changePassword) throws ServiceException {
        try {
            userService.verifyAndChangePassword(changePassword);
            return new ResponseEntity<>("Password changed.", HttpStatus.OK);
        } catch (ServiceException se) {
            return ErrorMessages.generateError(se.getError());
        }catch (Exception e) {
            return ErrorMessages.generateError(e);
        }
    }


    /**
     * Create new users
     *
     * @param changePassword
     * @return ResponseEntity
     */
    @Operation(summary = "Change password", description = "Change password")
    @PostMapping("/changepassword")
    @MonitorApp
    public ResponseEntity<Object> changepassword(@RequestBody ChangePassword changePassword) throws ServiceException {
        try {
            userService.changePassword(changePassword);
            return new ResponseEntity<>("Password updated.", HttpStatus.OK);
        } catch (ServiceException se) {
            return ErrorMessages.generateError(se.getError());
        }catch (Exception e) {
            return ErrorMessages.generateError(e);
        }
    }

    /**
     * Update users
     *
     * @param usersVO
     * @return ResponseEntity
     */
    @Operation(summary = "Update users", description = "Update user values")
    @PutMapping("/updateUsers")
    @MonitorApp
    public ResponseEntity<Object> updateUsers(@RequestBody UsersVO usersVO) throws ServiceException {
        try {
            userService.updateUser(usersVO);
            return new ResponseEntity<>("User updated.", HttpStatus.OK);
        } catch (ServiceException se) {
            return ErrorMessages.generateError(se.getError());
        }catch (Exception e) {
            return ErrorMessages.generateError(e);
        }
    }

}
