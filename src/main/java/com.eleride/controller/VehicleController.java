package com.eleride.controller;

import com.eleride.common.ErrorMessages;
import com.eleride.common.ServiceException;
import com.eleride.monitor.MonitorApp;
import com.eleride.service.PinDetailsService;
import com.eleride.service.VehicleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.PathParam;

@Slf4j
@RestController
@Tag(name = "Vehicle", description = "Vehicle")
@CrossOrigin
public class VehicleController {


    @Autowired
    VehicleService vehicleService;


    /**
     * Get all the vehicles
     *
     * @return ResponseEntity
     */
    @Operation(summary = "Get all vehicles by city and pin", description = "Get all vehicle with city and pin values.")
    @GetMapping("/getVehilesByCityAndPin/{cityId}/{pincode}")
    @MonitorApp
    public ResponseEntity<Object> getAllVehiclesByCity(@PathParam("cityId") long cityId,@PathParam("pincode") int pincode ) {
        try {
            return new ResponseEntity<>(vehicleService.getAllVehiclesByCityAndPin(cityId, pincode), HttpStatus.OK);
        }  catch (ServiceException se) {
            return ErrorMessages.generateError(se.getError());
        }catch (Exception e) {
            return ErrorMessages.generateError(e);
        }
    }

}
