package com.eleride.controller;

import com.eleride.common.ErrorMessages;
import com.eleride.common.ServiceException;
import com.eleride.monitor.MonitorApp;
import com.eleride.request.AddressVO;
import com.eleride.request.SubscriptionVO;
import com.eleride.service.AddressService;
import com.eleride.service.SubscriptionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.PathParam;

@Slf4j
@RestController
@Tag(name = "Subscription", description = "Subscription")
@CrossOrigin
public class SubscriptionController {


    @Autowired
    SubscriptionService subscriptionService;

    /**
     * Save subscriptions
     *
     * @return ResponseEntity
     */
    @Operation(summary = "Save subscription", description = "Save subscription data for a user")
    @PostMapping("/saveSubscription")
    @MonitorApp
    public ResponseEntity<Object> saveSubscription(@RequestBody SubscriptionVO subscriptionVO) {
        try {
            return new ResponseEntity<>(subscriptionService.saveSubscription(subscriptionVO), HttpStatus.OK);
        }  catch (ServiceException se) {
            return ErrorMessages.generateError(se.getError());
        }catch (Exception e) {
            return ErrorMessages.generateError(e);
        }
    }

    /**
     * Update subscriptions
     *
     * @return ResponseEntity
     */
    @Operation(summary = "Update subscription", description = "Update subscription data for a user")
    @PutMapping("/updateSubscription")
    @MonitorApp
    public ResponseEntity<Object> updateSubscription(@RequestBody SubscriptionVO subscriptionVO) {
        try {
            return new ResponseEntity<>(subscriptionService.updateSubscription(subscriptionVO), HttpStatus.OK);
        }  catch (ServiceException se) {
            return ErrorMessages.generateError(se.getError());
        }catch (Exception e) {
            return ErrorMessages.generateError(e);
        }
    }

    /**
     * Get all the subscription for a user
     *
     * @return ResponseEntity
     */
    @Operation(summary = "Get all subscription for a user", description = "Get all subscription for a user")
    @GetMapping("/getSubscriptions/{userId}")
    @MonitorApp
    public ResponseEntity<Object> getAllSubscriptions(@PathParam("userId") long userId) {
        try {
            return new ResponseEntity<>(subscriptionService.getAllSubscriptions(userId), HttpStatus.OK);
        }  catch (ServiceException se) {
            return ErrorMessages.generateError(se.getError());
        }catch (Exception e) {
            return ErrorMessages.generateError(e);
        }
    }
}
