package com.eleride.controller;

import com.eleride.common.ErrorMessages;
import com.eleride.common.ServiceException;
import com.eleride.monitor.MonitorApp;
import com.eleride.service.PinDetailsService;
import com.eleride.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@Tag(name = "Pin city details", description = "Pin city details")
@CrossOrigin
public class PinDetailsController {


    @Autowired
    PinDetailsService pinDetailsService;

    /**
     * Get all the employees
     *
     * @return ResponseEntity
     */
    @Operation(summary =  "Get all city and pins", description = "Get all city with pin values.")
    @GetMapping("/getAllCity")
    @MonitorApp
    public ResponseEntity<Object> getAllCity() {
        try {
            return new ResponseEntity<>(pinDetailsService.getAllCity(), HttpStatus.OK);
        }  catch (ServiceException se) {
            return ErrorMessages.generateError(se.getError());
        }catch (Exception e) {
            return ErrorMessages.generateError(e);
        }
    }


}
