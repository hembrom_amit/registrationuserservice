package com.eleride.controller;

import com.eleride.common.ErrorMessages;
import com.eleride.common.ServiceException;
import com.eleride.monitor.MonitorApp;
import com.eleride.request.AddressVO;
import com.eleride.request.SendOTPRequest;
import com.eleride.service.AddressService;
import com.eleride.service.PinDetailsService;
import com.eleride.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.PathParam;

@Slf4j
@RestController
@Tag(name = "Address", description = "Address")
@CrossOrigin
public class AddressController {


    @Autowired
    AddressService addressService;


    /**
     * Get all the employees
     *
     * @return ResponseEntity
     */
    @Operation(summary = "Save address", description = "Save address for a user")
    @PostMapping("/saveAddress")
    @MonitorApp
    public ResponseEntity<Object> saveAddress(@RequestBody AddressVO addressVO) {
        try {
            return new ResponseEntity<>(addressService.saveAddress(addressVO), HttpStatus.OK);
        }  catch (ServiceException se) {
            return ErrorMessages.generateError(se.getError());
        }catch (Exception e) {
            return ErrorMessages.generateError(e);
        }
    }

    /**
     * Get all the address for a user
     *
     * @return ResponseEntity
     */
    @Operation(summary = "Get all address for a user", description = "Get all address for a user")
    @GetMapping("/getAddress/{userId}")
    @MonitorApp
    public ResponseEntity<Object> getAllAddress(@PathParam("userId") long userId) {
        try {
            return new ResponseEntity<>(addressService.getAllAddress(userId), HttpStatus.OK);
        }  catch (ServiceException se) {
            return ErrorMessages.generateError(se.getError());
        }catch (Exception e) {
            return ErrorMessages.generateError(e);
        }
    }


}
