package com.eleride.controller;

import com.eleride.common.ErrorMessages;
import com.eleride.common.ServiceException;
import com.eleride.monitor.MonitorApp;
import com.eleride.request.RegistrationRequest;
import com.eleride.request.SendOTPRequest;
import com.eleride.responses.RegistrationResponse;
import com.eleride.service.RegistrationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@Slf4j
@RestController
@Tag(name = "Registration", description = "Registration")
@CrossOrigin
public class RegistrationController {
    @Autowired
    RegistrationService registrationService;


    /**
     * Create new users
     *
     * @param registrationRequest
     * @return ResponseEntity
     */
    @Operation(summary = "Register a new user", description = "Registration process")
    @PostMapping("/registration")
    @MonitorApp
    public ResponseEntity<Object> registration(@RequestBody RegistrationRequest registrationRequest)  {
        try {
            RegistrationResponse registrationResponse = null;
            if(registrationRequest.getIsSocialUser()){
                registrationResponse = registrationService.saveSocialUser(registrationRequest);
            } else {
                registrationResponse = registrationService.saveUser(registrationRequest);
            }

            return new ResponseEntity<>(registrationResponse, HttpStatus.OK);
        } catch (ServiceException se) {
            return ErrorMessages.generateError(se.getError());
        } catch (Exception e) {
            log.error("Exception "+e.getMessage());
            return ErrorMessages.generateError(e);
        }
    }



    /**
     * Create new users
     *
     * @param sendOTPRequest
     * @return ResponseEntity
     */
    @Operation(summary = "Send OTP", description = "Send OTP")
    @PostMapping("/resendotp")
    @MonitorApp
    public ResponseEntity<Object> resendOTP(@RequestBody SendOTPRequest sendOTPRequest) throws ServiceException {
        try {
            RegistrationResponse registrationResponse = registrationService.sendOTP(sendOTPRequest);
            return new ResponseEntity<>(registrationResponse, HttpStatus.OK);
        } catch (ServiceException se) {
            return ErrorMessages.generateError(se.getError());
        }catch (Exception e) {
            return ErrorMessages.generateError(e);
        }
    }
}
