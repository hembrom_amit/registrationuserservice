package com.eleride.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "vehicles")
public class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "vehicle_name")
    private String vehicleName;

    @Column(name = "vehicle_type")
    private String vehicleType;

    @Column(name = "company_name")
    private String companyName;

    @Column(name = "model_year")
    private String modelYear;

    @Column(name = "mainimage")
    private String mainImage;

    @Column(name = "allimages")
    private String allImages;

    @Column(name = "top_speed")
    private long topSpeed;

    @Column(name = "features")
    private String features;

    @Column(name = "description")
    private String description;


    @Column(name = "price_per_month")
    private long pricePerMonth;
}
