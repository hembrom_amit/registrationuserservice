package com.eleride.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "forgotpassword_otp")
public class ForgotPasswordOTP {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "user_id")
    private long userId;

    @Column(name = "temppassword")
    private String temporaryPassword;

    @Column(name = "status")
    private String status;

    @Column(name = "insertdate")
    private Timestamp insertDate;
}
