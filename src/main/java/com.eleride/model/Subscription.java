package com.eleride.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "subscription")
public class Subscription {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "user_id")
    private long userId;

    @Column(name = "vehicle_id")
    private long vehicleId;

    @Column(name = "address_id")
    private long addressId;

    @Column(name = "isDLVerified")
    private boolean isDLVerified;

    @Column(name = "price_per_month")
    private long pricePerMonth;

    @Column(name = "payment_status")
    private boolean paymentStatus;

    @Column(name = "payment_id")
    private long paymentId;

    @Column(name = "start_date")
    private Timestamp startDate;

    @Column(name = "end_date")
    private Timestamp endDate;
}
