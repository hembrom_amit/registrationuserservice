package com.eleride.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "pin_details")
public class PinDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "city_id")
    private long cityId;

    @Column(name = "office_name")
    private String officeName;

    @Column(name = "division_name")
    private String divisionName;

    @Column(name = "pincode")
    private int pincode;
}
