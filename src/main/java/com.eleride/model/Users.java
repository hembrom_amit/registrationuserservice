package com.eleride.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "users")
public class Users {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "password")
	private String password;

	@Column(name = "name")
	private String name;

	@Column(name = "email")
	private String email;

	@Column(name = "mobile")
	private long mobile;

	@Column(name = "gender")
	private String gender;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "dl_value")
	private String dlValue;

	@Column(name = "dl_verified")
	private Boolean dlVerified;

	@Column(name = "is_social_user")
	private Boolean isSocialUser;

	@Column(name = "social_user_type")
	private String socialUserType;

}
