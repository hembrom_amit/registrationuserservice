package com.eleride.repository;

import com.eleride.model.ForgotPasswordOTP;
import com.eleride.model.RegistrationOTP;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ForgotPasswordOTPRepository extends JpaRepository<ForgotPasswordOTP, Long> {
    List<ForgotPasswordOTP> findByUserIdAndTemporaryPassword(long userId, String tempPassword);
}
