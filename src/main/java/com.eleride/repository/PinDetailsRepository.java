package com.eleride.repository;

import com.eleride.model.PinDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PinDetailsRepository extends JpaRepository<PinDetails, Long> {
    List<PinDetails> findByCityId(int cityId);
}
