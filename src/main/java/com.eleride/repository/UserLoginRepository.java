package com.eleride.repository;

import com.eleride.model.UserLogin;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserLoginRepository extends JpaRepository<UserLogin, Long> {
    List<UserLogin> getByUserId(long userId);
}
