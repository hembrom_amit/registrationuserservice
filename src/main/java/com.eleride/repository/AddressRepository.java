package com.eleride.repository;

import com.eleride.model.Address;
import com.eleride.model.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AddressRepository extends JpaRepository<Address, Long> {
    List<Address> findByUserId(long userId);
    Address findById(long id);
    List<Address> findByIdIn(List<Long> addressIds);
}
