package com.eleride.repository;

import com.eleride.model.City;
import com.eleride.model.CityVehicle;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CityVehicleRepository extends JpaRepository<CityVehicle, Long> {
    List<CityVehicle> findByCityIdAndIsAvailable(long cityId, boolean isAvailable);
}
