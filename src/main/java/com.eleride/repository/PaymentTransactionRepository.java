package com.eleride.repository;

import com.eleride.model.PaymentTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface PaymentTransactionRepository extends JpaRepository<PaymentTransaction, Long> {
    List<PaymentTransaction> findByUserId(long userId);
    PaymentTransaction findById(long id);
    List<PaymentTransaction> findByIdIn(List<Long> paymentIds);

    PaymentTransaction findByTransactionId(String transactionId);
}
