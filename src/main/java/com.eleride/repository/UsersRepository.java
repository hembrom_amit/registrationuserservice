package com.eleride.repository;

import com.eleride.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<Users, Long> {

    Users findById(long id);

    Users findByEmailAndPassword(String email, String password);

    Users findByEmail(String email);

    Users findByMobile(long mobile);

    Users findByEmailOrMobile(String email, long mobile);
}
