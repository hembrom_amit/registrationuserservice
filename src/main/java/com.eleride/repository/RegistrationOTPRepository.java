package com.eleride.repository;

import com.eleride.model.RegistrationOTP;
import com.eleride.model.UserLogin;
import com.eleride.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RegistrationOTPRepository extends JpaRepository<RegistrationOTP, Long> {
    List<RegistrationOTP> findByUserIdAndOtp(long userId, String otp);
}
