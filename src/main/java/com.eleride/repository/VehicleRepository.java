package com.eleride.repository;

import com.eleride.model.City;
import com.eleride.model.Users;
import com.eleride.model.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VehicleRepository extends JpaRepository<Vehicle, Long> {
    List<Vehicle> findByIdIn(List<Long> vehicleIds);
    Vehicle findById(long id);
}
