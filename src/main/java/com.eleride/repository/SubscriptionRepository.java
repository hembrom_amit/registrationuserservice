package com.eleride.repository;

import com.eleride.model.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {
    List<Subscription> findByUserId(long userId);
    Subscription findById(long id);
    Subscription findByPaymentId(long paymentId);
}
