package com.eleride.service;

import com.eleride.common.ServiceException;
import com.eleride.model.City;
import com.eleride.model.PinDetails;
import com.eleride.repository.CityRepository;
import com.eleride.repository.PinDetailsRepository;
import com.eleride.responses.PinDetailsResponse;
import com.eleride.responses.PinDetailsVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class PinDetailsService {

    @Autowired
    PinDetailsRepository pinDetailsRepository;

    @Autowired
    CityRepository cityRepository;

    public Object getAllCity() {
        Map<Long,String> cityMap = getCityMap();
        if(cityMap==null || cityMap.isEmpty()){
            throw new ServiceException(Response.Status.BAD_REQUEST, "No city found in the database.");
        }
        log.info("CityMap == "+cityMap.toString());
        List<PinDetailsResponse> pinDetailsResponseList = new ArrayList<>();
        try{
            if(cityMap!=null && !cityMap.isEmpty()){
                List<PinDetails> pinDetailsList = pinDetailsRepository.findAll();
                Map<String, List<PinDetailsVO>> pinDetailsMap = new HashMap<>();
                List<PinDetailsVO> pinDetailsListTemp = null;
                String key = "";
                if(pinDetailsList!=null && !pinDetailsList.isEmpty()){
                    log.info("pin details list size == "+pinDetailsList.size());
                    for(PinDetails pinDetail : pinDetailsList){
                        //log.info("City == "+cityMap.get(pinDetail.getCityId())+" for city id = "+pinDetail.getCityId());
                        key = cityMap.get(pinDetail.getCityId());
                        //log.info("Key == "+key);
                        if(pinDetailsMap.containsKey(cityMap.get(pinDetail.getCityId()))){
                            pinDetailsListTemp = pinDetailsMap.get(cityMap.get(pinDetail.getCityId()));
                        } else {
                            pinDetailsListTemp = new ArrayList<>();
                        }
                        //log.info("pinDetailsListTemp size == "+pinDetailsListTemp.size());
                        pinDetailsListTemp.add(PinDetailsVO.builder().officeName(pinDetail.getOfficeName()).pincode(pinDetail.getPincode()).cityId(pinDetail.getCityId()).build());
                        pinDetailsMap.put(cityMap.get(pinDetail.getCityId()), pinDetailsListTemp);
                    }
                    for(String city : pinDetailsMap.keySet()){
                        PinDetailsResponse pinDetailsResponse = PinDetailsResponse.builder()
                                .cityId(pinDetailsMap.get(city).get(0).getCityId())
                                .city(city)
                                .pinDetailsList(pinDetailsMap.get(city))
                                .build();
                        pinDetailsResponseList.add(pinDetailsResponse);
                    }
                }
            }
        } catch(Exception e){
            e.printStackTrace();
        }
        return pinDetailsResponseList;
    }

    public Map<Long, String> getCityMap() {
        Map<Long,String> cityMap = null;
        log.info("Finind all city "+cityRepository);
        List<City> cityList = cityRepository.findAll();
        if(cityList!=null && !cityList.isEmpty()){
            cityMap= new HashMap<>();
            for(City city : cityList){
                cityMap.put(city.getId(),city.getName());
            }
        }
        return cityMap;
    }

    public Map<Long,List<Integer>> getCityPincodeMap(){
        List<PinDetails> pinDetailsList = pinDetailsRepository.findAll();
        List<Integer> pinDetailsListTemp = null;
        String key = "";
        Map<Long, List<Integer>> cityPincodeMap = new HashMap<>();
        if(pinDetailsList!=null && !pinDetailsList.isEmpty()){
            for(PinDetails pinDetail : pinDetailsList){
                if(cityPincodeMap.containsKey(pinDetail.getCityId())){
                    pinDetailsListTemp = cityPincodeMap.get(pinDetail.getCityId());
                } else {
                    pinDetailsListTemp = new ArrayList<>();
                }
                pinDetailsListTemp.add(pinDetail.getPincode());
                cityPincodeMap.put(pinDetail.getCityId(), pinDetailsListTemp);
            }
        }
        return cityPincodeMap;
    }
}
