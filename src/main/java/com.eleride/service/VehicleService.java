package com.eleride.service;

import com.eleride.common.PasswordManager;
import com.eleride.common.ServiceException;
import com.eleride.model.*;
import com.eleride.repository.*;
import com.eleride.request.LoginRequest;
import com.eleride.responses.CityVehicleVO;
import com.eleride.responses.LoginResponse;
import com.eleride.responses.VehicleVO;
import com.fasterxml.uuid.Generators;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Timestamp;
import java.util.*;

@Slf4j
@Service
public class VehicleService {

    @Autowired
    VehicleRepository vehicleRepository;

    @Autowired
    CityVehicleRepository cityVehicleRepository;

    @Autowired
    CityRepository cityRepository;

    @Autowired
    PinDetailsService pinDetailsService;

    List<Long> blockedPinCode = new ArrayList<>();



    public Object getAllVehiclesByCityAndPin(long cityId, int pincode) {
        Map<Long,String> cityMap = pinDetailsService.getCityMap();

        if (cityMap == null) {
            throw new ServiceException(Response.Status.BAD_REQUEST, "No city found in the database");
        }
        log.info("City id = "+cityId+" pincode = "+pincode);
        // Verify citid
        if(!cityMap.containsKey(cityId)){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Cityid "+cityId+" is invalid");
        }
        // Check if pincode belong to the city
        Map<Long, List<Integer>> cityPincodeMap = pinDetailsService.getCityPincodeMap();
        if(cityPincodeMap != null && cityPincodeMap.containsKey(cityId) && !cityPincodeMap.get(cityId).contains(pincode)){
            throw new ServiceException(Response.Status.BAD_REQUEST, "This pincode "+pincode+" does not belong to the city provided : "+cityMap.get(cityId));
        }
        // Blacklist pincode error throw
        if(blockedPinCode!=null && !blockedPinCode.isEmpty() && blockedPinCode.contains(pincode)){
            throw new ServiceException(Response.Status.BAD_REQUEST, "This pincode "+pincode+" belongs to blocked pincode.");
        }
        List<VehicleVO> cityVehicleVOList = new ArrayList<>();
        List<String> allImages = new ArrayList<>();
        List<CityVehicle> cityVehicleList = cityVehicleRepository.findByCityIdAndIsAvailable(cityId, true);
        List<Long> vehicleIds = getVehiclesIds(cityVehicleList);
        List<Vehicle> allVehicles = vehicleRepository.findByIdIn(vehicleIds);
        Map<Long,Vehicle> allVehicleMapByVehicleId =  convertListToMap(allVehicles);

        if(cityVehicleList !=null && !cityVehicleList.isEmpty()){
            for(CityVehicle cityVehicle : cityVehicleList){
                Vehicle vehicle = allVehicleMapByVehicleId.get(cityVehicle.getVehicleId());
                allImages = Arrays.asList(vehicle.getAllImages().split(","));
                cityVehicleVOList.add(VehicleVO.builder().id(vehicle.getId())
                        .vehicleName(vehicle.getVehicleName())
                        .companyName(vehicle.getCompanyName()).vehicleType(vehicle.getVehicleType()).mainImage(vehicle.getMainImage())
                        .allImages(allImages).description(vehicle.getDescription()).topSpeed(vehicle.getTopSpeed())
                        .features(vehicle.getFeatures()).pricePerMonth(vehicle.getPricePerMonth()).modelYear(vehicle.getModelYear())
                        .build());
            }
        }
        CityVehicleVO cityVehicleVO =  CityVehicleVO.builder()
                .cityName(cityMap.get(cityId)).totalVehicleCount(cityVehicleList.size())
                .vehiclesList(cityVehicleVOList).build();
        return cityVehicleVO;

    }


    public Map<Long, Vehicle> convertListToMap(List<Vehicle> allVehicles) {
        Map<Long, Vehicle> vehicleMapByVehicleId = new HashMap<>();
        for(Vehicle vehicle : allVehicles){
            vehicleMapByVehicleId.put(vehicle.getId(),vehicle);
        }
        return vehicleMapByVehicleId;
    }

    private List<Long> getVehiclesIds(List<CityVehicle> cityVehicleList) {
        List<Long> vehicleList = new ArrayList<>();
        for(CityVehicle cityVehicle : cityVehicleList){
            vehicleList.add(cityVehicle.getVehicleId());
        }
        return vehicleList;
    }

}
