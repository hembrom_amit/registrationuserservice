package com.eleride.service;

import com.eleride.common.PasswordManager;
import com.eleride.common.ServiceException;
import com.eleride.model.Address;
import com.eleride.model.UserLogin;
import com.eleride.model.Users;
import com.eleride.model.Vehicle;
import com.eleride.repository.AddressRepository;
import com.eleride.repository.UserLoginRepository;
import com.eleride.repository.UsersRepository;
import com.eleride.request.AddressVO;
import com.eleride.request.LoginRequest;
import com.eleride.responses.LoginResponse;
import com.fasterxml.uuid.Generators;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class AddressService {

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    UsersRepository usersRepository;

    public Object saveAddress(AddressVO addressVO) {
        verifyAddress(addressVO);
        Address addressDB = addressRepository.save(Address.builder().firstname(addressVO.getFirstname())
                .lastname(addressVO.getLastname()).societyDetails(addressVO.getSocietyDetails())
                .address1(addressVO.getAddress1()).address2(addressVO.getAddress2())
                .landmark(addressVO.getLandmark()).city(addressVO.getCity())
                .state(addressVO.getState()).pincode(addressVO.getPincode()).userId(addressVO.getUserId())
                .mobile(addressVO.getMobile()).build()) ;
        addressVO.setAddressId(addressDB.getId());
        return addressVO;
    }

    private void verifyAddress(AddressVO addressVO) {
        if(addressVO==null || addressVO.getPincode()==0 || addressVO.getCity()==null){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Address, pincode or city is empty");
        }
        if(addressVO.getFirstname() == null || addressVO.getLastname()==null){
            throw new ServiceException(Response.Status.BAD_REQUEST, "First name , last name is empty");
        }
        if(addressVO.getMobile() == 0 ){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Contact number is empty");
        }

        if(addressVO.getUserId() == 0 || usersRepository.findById(addressVO.getUserId())== null){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Invalid userid");
        }
    }

    public Object getAllAddress(long userId) {
        if(usersRepository.findById(userId)== null){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Invalid userid");
        }
        List<Address> addressList = addressRepository.findByUserId(userId);
        return  addressList;
    }

    public Map<Long, Address> convertListToMap(List<Address> allAddress) {
        Map<Long, Address> addressMapByAddressId = new HashMap<>();
        for(Address address : allAddress){
            addressMapByAddressId.put(address.getId(),address);
        }
        return addressMapByAddressId;
    }

}
