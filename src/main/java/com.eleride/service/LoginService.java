package com.eleride.service;

import com.eleride.common.PasswordManager;
import com.eleride.common.ServiceException;
import com.eleride.model.UserLogin;
import com.eleride.model.Users;
import com.eleride.repository.UserLoginRepository;
import com.eleride.repository.UsersRepository;
import com.eleride.request.LoginRequest;
import com.eleride.responses.LoginResponse;
import com.fasterxml.uuid.Generators;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Timestamp;
import java.util.List;

@Slf4j
@Service
public class LoginService {

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    UserLoginRepository loginRepository;

    @Autowired
    RegistrationService registrationService;

    @Autowired
    PasswordManager passwordUtility;


    public LoginResponse login(LoginRequest loginRequest) throws ServiceException {
        if(loginRequest.getEmail()==null || loginRequest.getEmail().isBlank()){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Email can not be blank.");
        }
        if(loginRequest.getPassword() ==null || loginRequest.getPassword().isBlank()){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Password can not be blank.");
        }
        Users userDB = usersRepository.findByEmail(loginRequest.getEmail());
        if (userDB == null) {
            throw new ServiceException(Response.Status.BAD_REQUEST, "Invalid login credentials.");
        }
        if(!userDB.getIsSocialUser()){
            boolean isValidPassword = false;
            try {
                isValidPassword = passwordUtility.validatePassword(loginRequest.getPassword(), userDB.getPassword());
            } catch (NoSuchAlgorithmException e) {
                log.error("Exception while generating strong password.", e);
            } catch (InvalidKeySpecException e) {
                log.error("Invalid key spec exception.", e);
            }
            if (!isValidPassword) {
                throw new ServiceException(Response.Status.BAD_REQUEST, "Invalid login credentials.");
            }
        }
        if(userDB.getIsActive()== false){
            registrationService.sendOTPRegisteredUser(userDB);
        }
        return loginAction(userDB, loginRequest.getEmail());
    }

    public LoginResponse loginAction(Users userDB, String email){
        String sessionId = Generators.timeBasedGenerator().generate().toString();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        logoutIfLogin(userDB.getId(), timestamp);
        UserLogin login = UserLogin.builder().userId(userDB.getId()).sessionId(sessionId).loginTime(timestamp).build();
        loginRepository.save(login);
        return LoginResponse.builder()
                .email(email)
                .userId(userDB.getId())
                .sessionId(sessionId)
                .name(userDB.getName())
                .mobile(userDB.getMobile())
                .dlValue(userDB.getDlValue())
                .dlVerified(userDB.getDlVerified())
                .gender(userDB.getGender())
                .isActive(userDB.getIsActive())
                .build();
    }

    private String getEncryptedPassword(String password) {
        try {
            return passwordUtility.generateStorngPasswordHash(password);
        }  catch (NoSuchAlgorithmException e) {
            log.error("Exception while generating strong password.", e);
        } catch (InvalidKeySpecException e) {
            log.error("Invalid key spec exception.", e);
        }
        throw new ServiceException(Response.Status.SERVICE_UNAVAILABLE, "Error in login process.");
    }

    private void logoutIfLogin(long userId, Timestamp timestamp) {
        List<UserLogin> userLogin = loginRepository.getByUserId(userId);
        if(userLogin!=null && userLogin.size()>0){
            UserLogin lastLoginEntry = userLogin.get(userLogin.size()-1);
            lastLoginEntry.setLogoutTime(timestamp);
            loginRepository.save(lastLoginEntry);
        }

    }

    public LoginResponse socialLogin(LoginRequest loginRequest) {
        Users userDB = usersRepository.findByEmail(loginRequest.getEmail());
        if (userDB == null) {
            throw new ServiceException(Response.Status.BAD_REQUEST, "Invalid login credentials.");
        }
        return loginAction(userDB, loginRequest.getEmail());
    }
}
