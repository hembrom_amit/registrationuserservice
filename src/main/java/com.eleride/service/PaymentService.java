package com.eleride.service;

import com.eleride.common.ServiceException;
import com.eleride.model.Address;
import com.eleride.model.PaymentTransaction;
import com.eleride.repository.AddressRepository;
import com.eleride.repository.PaymentTransactionRepository;
import com.eleride.repository.UsersRepository;
import com.eleride.request.AddressVO;
import com.eleride.request.PaymentTransactionVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class PaymentService {

    @Autowired
    PaymentTransactionRepository paymentTransactionRepository;

    @Autowired
    UserService userService;

    @Autowired
    SubscriptionService subscriptionService;

    private final Integer MAX_DEPOSIT_AMOUNT  =10000;
    private final Integer TRANSACTION_ID_LENGTH  =10;
    private final Integer MAX_RANDOMGENERATOR_ATTEMPT  =10000;
    private final String IN_PROGRESS  = "INPROGRESS";
    private final String SUCCESS  = "SUCCESS";
    private final String FAILED  = "FAILED";

    public PaymentTransactionVO paymentInitiation(PaymentTransactionVO paymentTransactionVO) {
        try{
            verifyPaymentInput(paymentTransactionVO);
            String transactionId = RandomStringUtils.randomAlphanumeric(TRANSACTION_ID_LENGTH).toUpperCase();
            int countRandomTxnGenerator=0;
            while(!verifyTransactionUniqueness(transactionId) && countRandomTxnGenerator < MAX_RANDOMGENERATOR_ATTEMPT){
                transactionId = RandomStringUtils.randomAlphanumeric(TRANSACTION_ID_LENGTH).toUpperCase();
            }
            PaymentTransaction paymentTransaction = paymentTransactionRepository.save(PaymentTransaction.builder().userId(paymentTransactionVO.getUserId())
                    .amount(paymentTransactionVO.getAmount()).transactionId(transactionId)
                    .status(IN_PROGRESS).build());
            paymentTransactionVO.setTransactionId(transactionId);
            paymentTransactionVO.setId(paymentTransaction.getId());
        } catch(Exception e){
            log.error("Something went wrong while processing payment initiation ",e.getMessage());
            throw e;
        }
        return paymentTransactionVO;
    }

    private boolean verifyTransactionUniqueness(String transactionId) {
        if(paymentTransactionRepository.findByTransactionId(transactionId) != null){
            return false;
        }
        return true;
    }

    private void verifyPaymentInput(PaymentTransactionVO paymentTransactionVO) {
        if(paymentTransactionVO == null){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Please enter payment details.");
        }

        if(paymentTransactionVO.getUserId()==0 || userService.getUser(paymentTransactionVO.getUserId())==null){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Invalid userId");
        }

        if(paymentTransactionVO.getAmount() == 0 || paymentTransactionVO.getAmount() > MAX_DEPOSIT_AMOUNT){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Payment amount should be between 1 and "+MAX_DEPOSIT_AMOUNT);
        }
    }

    public Object paymentUpdate(PaymentTransactionVO paymentTransactionVO) {
        try{
            verifyPaymentInput(paymentTransactionVO);
            PaymentTransaction paymentTransaction = paymentTransactionRepository.findByTransactionId(paymentTransactionVO.getTransactionId());
            if(paymentTransaction == null){
                throw new ServiceException(Response.Status.BAD_REQUEST, "Transaction id not found");
            }
            if(paymentTransactionVO.getStatus()!=null &&!paymentTransactionVO.getStatus().isBlank()){
                paymentTransaction.setStatus(paymentTransactionVO.getStatus());
            }
            if(paymentTransactionVO.getErrorText()!=null &&!paymentTransactionVO.getErrorText().isBlank()){
                paymentTransaction.setErrorText(paymentTransactionVO.getErrorText());
            }
            if(paymentTransactionVO.getErrorCodePG() !=null && paymentTransactionVO.getErrorCodePG()!=0){
                paymentTransaction.setErrorCodePG(paymentTransactionVO.getErrorCodePG());
            }
            paymentTransactionRepository.save(paymentTransaction);
            //Update subscription data
            subscriptionService.updateSubscriptionByPaymentId(paymentTransaction.getId());
        }catch (Exception e){
            log.error("Something went wrong while updating the payment process ",e.getMessage());
            throw e;
        }
        return paymentTransactionVO;
    }
}
