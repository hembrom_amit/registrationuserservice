package com.eleride.service;

import com.eleride.common.PasswordManager;
import com.eleride.common.ServiceException;
import com.eleride.model.RegistrationOTP;
import com.eleride.model.Users;
import com.eleride.repository.RegistrationOTPRepository;
import com.eleride.repository.UsersRepository;
import com.eleride.request.RegistrationRequest;
import com.eleride.request.SendOTPRequest;
import com.eleride.responses.RegistrationResponse;
import com.eleride.utility.SMSNotificationUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.core.Response;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Timestamp;
import java.util.Random;

@Slf4j
@Service
public class RegistrationService {

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    PasswordManager passwordUtility;

    @Autowired
    SMSNotificationUtility smsNotificationUtility;

    @Autowired
    RegistrationOTPRepository registrationOTPRepository;

    @Transactional
    public RegistrationResponse saveUser(RegistrationRequest registrationRequest)  {
        Users user = Users.builder().name(registrationRequest.getName()).mobile(registrationRequest.getMobile())
                        .email(registrationRequest.getEmail()).password(registrationRequest.getPassword()).isSocialUser(registrationRequest.getIsSocialUser())
                .socialUserType(registrationRequest.getSocialUserType()).build();
        log.info("user from registration request "+user.toString());
        Users userDB = usersRepository.findByEmailOrMobile(user.getEmail(), user.getMobile());
        checkForUniqueEmailAndMobileNumber(userDB, user);
        Users newUserDB = new Users();
        log.info("user "+user.toString());
        newUserDB = saveInUserTable(user);
        sendOTPRegisteredUser(newUserDB);
        return RegistrationResponse.builder().email(newUserDB.getEmail()).userId(newUserDB.getId()).name(newUserDB.getName()).mobile(newUserDB.getMobile()).build();
    }

    private void checkForUniqueEmailAndMobileNumber(Users userDB, Users user) {
        try{
            if (userDB != null ){
                String error = "";
                if(userDB.getEmail().equals(user.getEmail())){
                    error = "Email id already exist.Please select another email id.";
                    throw new ServiceException(Response.Status.BAD_REQUEST, error);
                }
                if(!user.getIsSocialUser() && userDB.getMobile()==user.getMobile()){
                    error = "Mobile no already exist.Please select another Mobile no.";
                    throw new ServiceException(Response.Status.BAD_REQUEST, error);
                }

            }
        } catch (Exception e){
            throw e;
        }
    }

    private Users saveInUserTable(Users user) {
        Users newUserDB = new Users();
        String password = "";
        boolean isActive = false;
        if(!user.getIsSocialUser()){
            try {
                password = passwordUtility.generateStorngPasswordHash(user.getPassword());
                user.setPassword(password);
            } catch (NoSuchAlgorithmException e) {
                log.error("Exception while generating strong password.", e);
            } catch (InvalidKeySpecException e) {
                log.error("Invalid key spec exception.", e);
            }
        } else {
            isActive = true;
        }

        try{
            newUserDB =usersRepository
                    .save(Users.builder()
                            .email(user.getEmail())
                            .mobile(user.getMobile())
                            .password(user.getPassword())
                            .dlValue(user.getDlValue())
                            .dlVerified(false)
                            .name(user.getName())
                            .gender(user.getGender())
                            .isActive(isActive)
                            .isSocialUser((user.getIsSocialUser()))
                            .socialUserType(user.getSocialUserType())
                            .build());
        } catch(Exception e){
            throw e;
        }
        return newUserDB;
    }

    public void sendOTPRegisteredUser(Users newUserDB) {
        String otpValue = getOTP();
        registrationOTPRepository.save(RegistrationOTP.builder().userId(newUserDB.getId()).otp(otpValue).status("INPROGRESS").insertDate(new Timestamp(System.currentTimeMillis())).build());
        smsNotificationUtility.sendRegistrationOTPSMS(newUserDB.getMobile()+"",otpValue);
    }

    private String getOTP() {
        Random random = new Random();
        String randomOTP = String.format("%04d", random.nextInt(10000));
        return randomOTP;
    }


    public RegistrationResponse saveSocialUser(RegistrationRequest registrationRequest) {
        Users user = Users.builder().name(registrationRequest.getName()).mobile(registrationRequest.getMobile())
                .email(registrationRequest.getEmail()).password(registrationRequest.getPassword()).isSocialUser(registrationRequest.getIsSocialUser())
                .socialUserType(registrationRequest.getSocialUserType()).build();
        log.info("user from registration request "+user.toString());
        Users userDB = usersRepository.findByEmail(user.getEmail());
        checkForUniqueEmailAndMobileNumber(userDB, user);
        Users newUserDB = new Users();
        newUserDB = saveInUserTable(user);
        return RegistrationResponse.builder().email(newUserDB.getEmail()).userId(newUserDB.getId()).name(newUserDB.getName()).mobile(newUserDB.getMobile()).build();
    }

    public RegistrationResponse sendOTP(SendOTPRequest sendOTPRequest) {

        Users userDB = usersRepository.findById(sendOTPRequest.getUserId());
        if(userDB!=null){
            if(sendOTPRequest.getMobile() != userDB.getMobile()){
                userDB.setMobile(sendOTPRequest.getMobile());
                usersRepository.save(userDB);
            }
            sendOTPRegisteredUser(userDB);
            return RegistrationResponse.builder().email(userDB.getEmail()).userId(userDB.getId()).name(userDB.getName()).mobile(userDB.getMobile()).build();
        }
        String error = "Invalid userid";
        throw new ServiceException(Response.Status.BAD_REQUEST, error);
    }

}
