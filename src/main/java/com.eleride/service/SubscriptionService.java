package com.eleride.service;

import com.eleride.common.ServiceException;
import com.eleride.model.Address;
import com.eleride.model.Subscription;
import com.eleride.model.Users;
import com.eleride.model.Vehicle;
import com.eleride.repository.AddressRepository;
import com.eleride.repository.SubscriptionRepository;
import com.eleride.repository.UsersRepository;
import com.eleride.repository.VehicleRepository;
import com.eleride.request.AddressVO;
import com.eleride.request.SubscriptionResponse;
import com.eleride.request.SubscriptionVO;
import com.eleride.responses.VehicleVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class SubscriptionService {

    @Autowired
    SubscriptionRepository subscriptionRepository;

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    VehicleRepository vehicleRepository;

    @Autowired
    VehicleService vehicleService;

    @Autowired
    AddressService addressService;

    @Autowired
    AddressRepository addressRepository;

    private static final long ONE_MONTH_IN_MS = 2629800000l;

    public Object saveSubscription(SubscriptionVO subscriptionVO) {
        validateSubscriptionData(subscriptionVO);
        Subscription subscriptionDB = subscriptionRepository.save(Subscription.builder().userId(subscriptionVO.getUserId())
                .vehicleId(subscriptionVO.getVehicleId())
                .addressId(subscriptionVO.getAddressId())
                .pricePerMonth(subscriptionVO.getPricePerMonth())
                .isDLVerified(subscriptionVO.isDLVerified())
                .startDate(new Timestamp(System.currentTimeMillis()))
                .endDate(new Timestamp(System.currentTimeMillis()+ONE_MONTH_IN_MS))
                .build());
        subscriptionVO.setId(subscriptionDB.getId());
        return subscriptionVO;
    }

    private void validateSubscriptionData(SubscriptionVO subscriptionVO) {
        if(subscriptionVO == null){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Subscription is empty");
        }

        if(subscriptionVO.getUserId() == 0){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Please enter the correct userId value");
        }
        if(usersRepository.findById(subscriptionVO.getUserId())== null){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Invalid userid");
        }
        if(vehicleRepository.findById(subscriptionVO.getVehicleId())== null){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Invalid vehicleid");
        }
    }

    public Object updateSubscription(SubscriptionVO subscriptionVO) {
        validateSubscriptionData(subscriptionVO);
        if(subscriptionVO.getId()==0){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Please enter the subscriptionid value");
        }
        Subscription subscriptionDB = subscriptionRepository.findById(subscriptionVO.getId());
        if(subscriptionDB==null){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Invalid subscriptionid found");
        }
        if(subscriptionVO.getPricePerMonth()>0){
            subscriptionDB.setPricePerMonth(subscriptionVO.getPricePerMonth());
        }
        Address address = addressRepository.findById(subscriptionVO.getAddressId());
        if(address==null){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Address id not found");
        }
        Users userDB = usersRepository.findById(subscriptionVO.getUserId());
        if(subscriptionVO.isDLVerified()){
            subscriptionDB.setDLVerified(true);
            // Update users details
            userDB.setDlVerified(true);
        }
        if(subscriptionVO.getDlValue()!=null && !subscriptionVO.getDlValue().isBlank()){
            userDB.setDlValue(subscriptionVO.getDlValue());
        }
        if(subscriptionVO.isDLVerified() || (subscriptionVO.getDlValue()!=null && !subscriptionVO.getDlValue().isBlank()) ){
            usersRepository.save(userDB);
        }
        if(subscriptionVO.getPaymentId() > 0){
            subscriptionDB.setPaymentId(subscriptionVO.getPaymentId());
        }
        if(subscriptionVO.isPaymentStatus()){
            subscriptionDB.setPaymentStatus(true);
        }
        if(subscriptionVO.getStartDate()!=null){
            subscriptionDB.setStartDate(subscriptionVO.getStartDate());
        } else {
            subscriptionDB.setStartDate(new Timestamp(System.currentTimeMillis()));
        }
        if(subscriptionVO.getEndDate()!=null){
            subscriptionDB.setEndDate(subscriptionVO.getEndDate());
        } else {
            subscriptionDB.setEndDate(new Timestamp(System.currentTimeMillis()+ONE_MONTH_IN_MS));
        }
        subscriptionRepository.save(subscriptionDB);
        return subscriptionVO;
    }

    public Object getAllSubscriptions(long userId) {
        if(userId==0 || usersRepository.findById(userId)== null){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Invalid userId found");
        }
        List<Subscription> subscriptionList = subscriptionRepository.findByUserId(userId);
        if(subscriptionList==null || subscriptionList.isEmpty()){
            return new ArrayList<Subscription>();
        }
        List<Long> addressIds = getAllAddressIds(subscriptionList);
        List<Long> vehicleIds = getAllVehicleIds(subscriptionList);
        List<Vehicle> allVehicles = vehicleRepository.findByIdIn(vehicleIds);
        List<Address> allAddress = addressRepository.findByIdIn(addressIds);
        Map<Long, Vehicle> vehicleMap = vehicleService.convertListToMap(allVehicles);
        Map<Long, Address> addressMap = addressService.convertListToMap(allAddress);
        return prepareSubsList(subscriptionList,vehicleMap,addressMap);
    }

    private Object prepareSubsList(List<Subscription> subscriptionList, Map<Long, Vehicle> vehicleMap, Map<Long, Address> addressMap) {
        List<SubscriptionResponse> subscriptionResponseList = new ArrayList<>();
        for(Subscription subscription : subscriptionList){
            SubscriptionResponse subscriptionResponse = new SubscriptionResponse();
            if(vehicleMap.containsKey(subscription.getVehicleId())){
                Vehicle vehicle = vehicleMap.get(subscription.getVehicleId());
                List<String> allImages = Arrays.asList(vehicle.getAllImages().split(","));
                subscriptionResponse.setVehicle(VehicleVO.builder().id(vehicle.getId())
                        .vehicleName(vehicle.getVehicleName())
                        .companyName(vehicle.getCompanyName()).vehicleType(vehicle.getVehicleType()).mainImage(vehicle.getMainImage())
                        .allImages(allImages).description(vehicle.getDescription()).topSpeed(vehicle.getTopSpeed())
                        .features(vehicle.getFeatures()).pricePerMonth(vehicle.getPricePerMonth()).modelYear(vehicle.getModelYear())
                        .build());
            }

            if(addressMap.containsKey(subscription.getAddressId())){
                subscriptionResponse.setAddress(addressMap.get(subscription.getAddressId()));
            }
            subscriptionResponse.setId(subscription.getId());
            subscriptionResponse.setUserId(subscription.getUserId());
            subscriptionResponse.setDLVerified(subscription.isDLVerified());
            subscriptionResponse.setPricePerMonth(subscription.getPricePerMonth());
            subscriptionResponse.setPaymentId(subscription.getPaymentId());
            subscriptionResponse.setPaymentStatus(subscription.isPaymentStatus());
            subscriptionResponse.setStartDate(subscription.getStartDate());
            subscriptionResponse.setEndDate(subscription.getEndDate());
            subscriptionResponseList.add(subscriptionResponse);
        }
        return subscriptionResponseList;
    }

    private List<Long> getAllVehicleIds(List<Subscription> subscriptionList) {
        List<Long> vehicleIds = new ArrayList<>();
        for(Subscription subscription : subscriptionList){
            if(subscription.getVehicleId()>0){
                vehicleIds.add(subscription.getVehicleId());
            }
        }
        return vehicleIds;
    }

    private List<Long> getAllAddressIds(List<Subscription> subscriptionList) {
        List<Long> addressIds = new ArrayList<>();
        for(Subscription subscription : subscriptionList){
            if(subscription.getAddressId()>0){
                addressIds.add(subscription.getAddressId());
            }
        }
        return addressIds;
    }

    public void updateSubscriptionByPaymentId(long paymentId) {
        if(paymentId==0){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Payment id value is invalid");
        }
        Subscription subscriptionDB = subscriptionRepository.findByPaymentId(paymentId);
        if(subscriptionDB==null){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Invalid subscriptionid found");
        }
        subscriptionDB.setPaymentStatus(true);
        subscriptionRepository.save(subscriptionDB);
    }
}
