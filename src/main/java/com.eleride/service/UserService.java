package com.eleride.service;

import com.eleride.common.PasswordManager;
import com.eleride.common.ServiceException;
import com.eleride.model.ForgotPasswordOTP;
import com.eleride.model.RegistrationOTP;
import com.eleride.model.UserLogin;
import com.eleride.model.Users;
import com.eleride.repository.ForgotPasswordOTPRepository;
import com.eleride.repository.RegistrationOTPRepository;
import com.eleride.repository.UsersRepository;
import com.eleride.request.ChangePassword;
import com.eleride.request.ForgotPassword;
import com.eleride.request.OTPRequest;
import com.eleride.request.UsersVO;
import com.eleride.utility.SMSNotificationUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;
import java.util.Random;

@Slf4j
@Service
public class UserService {

    @Autowired
    RegistrationOTPRepository registrationOTPRepository;

    @Autowired
    ForgotPasswordOTPRepository forgotPasswordOTPRepository;

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    SMSNotificationUtility smsNotificationUtility;

    @Autowired
    PasswordManager passwordUtility;

    private final long FIVE_MINUTE_IN_MS = 300000l;

    public List<Users> getAllUsers() {
        return usersRepository.findAll();
    }

    public List<Users> verifyOTP(OTPRequest otpRequest) {
        List<RegistrationOTP> registrationOTPList = registrationOTPRepository.findByUserIdAndOtp(otpRequest.getUserId(), otpRequest.getOtp());
        log.info("registrationOTPList == "+registrationOTPList.toString());
        if(registrationOTPList==null  || registrationOTPList.isEmpty()){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Invalid OTP value entered.");
        }

        if(registrationOTPList != null){
            Timestamp timeOTPSend = registrationOTPList.get(0).getInsertDate();
            log.info("OTP send time = "+timeOTPSend);
            log.info("Time diff = "+(System.currentTimeMillis()-timeOTPSend.getTime()));
            if((System.currentTimeMillis()-timeOTPSend.getTime()) > FIVE_MINUTE_IN_MS){
                throw new ServiceException(Response.Status.BAD_REQUEST, "OTP expired.");
            }
        }
        // Update User isactive as Active
        Users userDB = usersRepository.findById(otpRequest.getUserId());
        if (userDB == null) {
            throw new ServiceException(Response.Status.BAD_REQUEST, "UserId not found.");
        }
        userDB.setIsActive(true);
        usersRepository.save(userDB);
        // Update RegistrationOTP status as Active
        RegistrationOTP registrationOTP = registrationOTPList.get(registrationOTPList.size()-1);
        registrationOTP.setStatus("SUCCESS");
        registrationOTPRepository.save(registrationOTP);
        return usersRepository.findAll();
    }

    public Users forgotPassword(ForgotPassword forgotPassword) {
        if(forgotPassword.getMobile()==0){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Invalid mobile number entered.");
        }
        Users userDB = usersRepository.findByMobile(forgotPassword.getMobile());
        if (userDB == null ){
            String error = "Mobile number does not exist in our system.Please check.";
            throw new ServiceException(Response.Status.BAD_REQUEST, error);
        }
        sendTempPassword(userDB);
        return userDB;
    }

    private void sendTempPassword(Users userDB) {
        String forgotPasswordCode = getForgotPasswordCode();
        forgotPasswordOTPRepository.save(ForgotPasswordOTP.builder().userId(userDB.getId()).temporaryPassword(forgotPasswordCode).status("INPROGRESS").insertDate(new Timestamp(System.currentTimeMillis())).build());
        smsNotificationUtility.sendForgotPasswordSMS(userDB.getMobile()+"",forgotPasswordCode);
    }
    private String getForgotPasswordCode() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 6;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        return generatedString.toUpperCase(Locale.ROOT);
    }

    public void verifyAndChangePassword(ChangePassword changePassword) {
        checkOldAndNewPassword(changePassword.getOldPassword(), changePassword.getNewPassword());
        Users userDB = usersRepository.findById(changePassword.getUserId());
        if (userDB == null) {
            throw new ServiceException(Response.Status.BAD_REQUEST, "UserId not found.");
        }
        log.info(" userid "+changePassword.getUserId()+" old pass "+changePassword.getOldPassword());
        // Verify old password
        List<ForgotPasswordOTP> forgotpasswordOTPList = forgotPasswordOTPRepository.findByUserIdAndTemporaryPassword(changePassword.getUserId(), changePassword.getOldPassword());

        log.info("forgotpasswordOTPList "+forgotpasswordOTPList);
        if(forgotpasswordOTPList==null  || forgotpasswordOTPList.isEmpty()){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Invalid temporary password entered.");
        }
        ForgotPasswordOTP forgotPasswordOTP = forgotpasswordOTPList.get(forgotpasswordOTPList.size()-1);
        log.info("changePassword.getOldPassword() equals "+(changePassword.getOldPassword().equals(forgotPasswordOTP.getTemporaryPassword())));
        if(!changePassword.getOldPassword().equals(forgotPasswordOTP.getTemporaryPassword())){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Invalid temporary password entered.");
        }
        if(forgotpasswordOTPList != null){
            Timestamp timeOTPSend = forgotPasswordOTP.getInsertDate();
            log.info("OTP send time = "+timeOTPSend);
            log.info("Time diff = "+(System.currentTimeMillis()-timeOTPSend.getTime()));
            if((System.currentTimeMillis()-timeOTPSend.getTime()) > 300000 ){
                throw new ServiceException(Response.Status.BAD_REQUEST, "Invalid temporary password expired.");
            }
            if(forgotPasswordOTP.getStatus().equals("SUCCESS") ){
                throw new ServiceException(Response.Status.BAD_REQUEST, "Password changed already for a given temporary password. Generate a new temp password again.");
            }
        }
        // Save new password
        try {
            String password = passwordUtility.generateStorngPasswordHash(changePassword.getNewPassword());
            userDB.setPassword(password);
        } catch (NoSuchAlgorithmException e) {
            log.error("Exception while generating strong password.", e);
        } catch (InvalidKeySpecException e) {
            log.error("Invalid key spec exception.", e);
        }
        usersRepository.save(userDB);
        // Update ForgotPasswordOTP status as Active
        forgotPasswordOTP.setStatus("SUCCESS");
        forgotPasswordOTPRepository.save(forgotPasswordOTP);
    }

    private void checkOldAndNewPassword(String oldPassword, String newPassword) {
        if(oldPassword.equals(newPassword)){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Old and New password must be different.");
        }
    }

    public void changePassword(ChangePassword changePassword) {
        checkOldAndNewPassword(changePassword.getOldPassword(), changePassword.getNewPassword());
        Users userDB = usersRepository.findById(changePassword.getUserId());
        if (userDB == null) {
            throw new ServiceException(Response.Status.BAD_REQUEST, "UserId not found.");
        }
        boolean isValidPassword = false;
        try {
            isValidPassword = passwordUtility.validatePassword(changePassword.getOldPassword(), userDB.getPassword());
        } catch (NoSuchAlgorithmException e) {
            log.error("Exception while generating strong password.", e);
        } catch (InvalidKeySpecException e) {
            log.error("Invalid key spec exception.", e);
        }
        if (!isValidPassword) {
            throw new ServiceException(Response.Status.BAD_REQUEST, "Invalid old password entered.");
        }
        try {
            String password = passwordUtility.generateStorngPasswordHash(changePassword.getNewPassword());
            userDB.setPassword(password);
        } catch (NoSuchAlgorithmException e) {
            log.error("Exception while generating strong password.", e);
        } catch (InvalidKeySpecException e) {
            log.error("Invalid key spec exception.", e);
        }
        usersRepository.save(userDB);
    }

    public UsersVO updateUser(UsersVO usersVO) {
        if(usersVO.getUserId()==0){
            throw new ServiceException(Response.Status.BAD_REQUEST, "Invalid userId");
        }
        Users userDB = usersRepository.findById(usersVO.getUserId());
        if(userDB==null){
            throw new ServiceException(Response.Status.BAD_REQUEST, "UserId not found");
        }
        log.info("EMail == "+usersVO.getEmail());
        if(usersVO.getEmail() != null &&  !usersVO.getEmail().isBlank()) {
            userDB.setEmail(usersVO.getEmail());
        }
        log.info("Mobile == "+usersVO.getMobile());
        if(usersVO.getMobile() > 0) {
            userDB.setMobile(usersVO.getMobile());
        }
        log.info("Name == "+usersVO.getName());
        if(usersVO.getName() != null && !usersVO.getName().isBlank()) {
            userDB.setName(usersVO.getName());
        }
        log.info("DLVerified == "+usersVO.getDlVerified());
        if(usersVO.getDlVerified() != null && usersVO.getDlVerified()) {
            userDB.setDlVerified(true);
        }
        log.info("DLValue == "+usersVO.getDlValue());
        if(usersVO.getDlVerified() != null && usersVO.getDlVerified() && usersVO.getDlValue() !=null && !usersVO.getDlValue().isBlank() ) {
            userDB.setDlValue(usersVO.getDlValue());
        }
        usersRepository.save(userDB);
        return usersVO;
    }

    public Users getUser(long userId){
        return usersRepository.findById(userId);
    }
}
