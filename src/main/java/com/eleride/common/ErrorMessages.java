package com.eleride.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Slf4j
public class ErrorMessages {
    public static ResponseEntity<Object> generateError(Exception e) {
        if(e instanceof DataIntegrityViolationException){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error while doing database operation.");
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Could not process the request.Please try after some time.\nError Message found : "+e.getMessage());
    }

    public static ResponseEntity<Object> generateError(String errorMessage) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessage);
    }

}
