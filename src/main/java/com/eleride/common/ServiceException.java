package com.eleride.common;

import javax.ws.rs.core.Response;
import java.util.HashMap;


public class ServiceException extends RuntimeException{

    private static final long serialVersionUID = 1L;
    private Response.Status status;
    private Integer statusCode;
    private String error;
    private Integer businessErrorCode=0;
    private HashMap<String,Object> keyValuemap;
    
    public Response.Status getStatus() {
        return status;
    }
    public void setStatus(Response.Status status) {
        this.status = status;
    }
    public String getError() {
        return error;
    }
    public void setError(String error) {
        this.error = error;
    }
    public Integer getBusinessErrorCode() {
        return businessErrorCode;
    }
    public void setBusinessErrorCode(Integer businessErrorCode) {
        this.businessErrorCode = businessErrorCode;
    }
    
    public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	
	public ServiceException(Integer statusCode, String error,
			Integer businessErrorCode) {
		super(error);
		this.statusCode = statusCode;
		this.error = error;
		this.businessErrorCode = businessErrorCode;
	}
	public ServiceException(Response.Status status, String error, Integer businessErrorCode) {
        super(error);
        this.status = status;
        this.error = error;
        this.businessErrorCode = businessErrorCode;
    }
	
	public ServiceException(Response.Status status, String error, Integer businessErrorCode, Exception e) {
        super(error,e);
        this.status = status;
        this.error = error;
        this.businessErrorCode = businessErrorCode;
    }
    public ServiceException(Response.Status status, String error) {
        super();
        this.status = status;
        this.error = error;
    }
    
    
	public ServiceException(Exception cause, Integer statusCode, String error,
			Integer businessErrorCode) {
		super(error);
		this.statusCode = statusCode;
		this.error = error;
		this.businessErrorCode = businessErrorCode;
	}
	public ServiceException(Response.Status status, Integer businessErrorCode) {
        super();
        this.status = status;
        this.businessErrorCode = businessErrorCode;
    }
	
	public HashMap<String, Object> getKeyValuemap() {
		return keyValuemap;
	}
	public void setKeyValuemap(HashMap<String, Object> keyValuemap) {
		this.keyValuemap = keyValuemap;
	}
	@Override
	public String toString() {
		return "ServiceException [status=" + status + ", statusCode="
				+ statusCode + ", error=" + error + ", businessErrorCode="
				+ businessErrorCode + "]";
	}
}
