package com.eleride.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UsersVO {
    private long userId;
    private String password;
    private String name;
    private String email;
    private long mobile;
    private String gender;
    private Boolean isActive;
    private String dlValue;
    private Boolean dlVerified;
    private Boolean isSocialUser;
    private String socialUserType;
}
