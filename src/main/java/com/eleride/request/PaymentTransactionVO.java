package com.eleride.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PaymentTransactionVO {
    private long id;
    private long userId;
    private String transactionId;
    private String status;
    private long amount;
    private Integer errorCodePG;
    private String errorText;
}
