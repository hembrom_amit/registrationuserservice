package com.eleride.request;


import com.eleride.model.Address;
import com.eleride.responses.VehicleVO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SubscriptionResponse {
    private long id;
    private long userId;
    private VehicleVO vehicle;
    private Address address;
    private boolean isDLVerified;
    private long pricePerMonth;
    private boolean paymentStatus;
    private long paymentId;
    private Timestamp startDate;
    private Timestamp endDate;
}
