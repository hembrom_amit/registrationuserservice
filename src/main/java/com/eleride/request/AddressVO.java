package com.eleride.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AddressVO {
    private long addressId;
    private long userId;
    private String firstname;
    private String lastname;
    private String societyDetails;
    private String address1;
    private String address2;
    private String landmark;
    private String city;
    private String state;
    private int pincode;
    private long mobile;
}
