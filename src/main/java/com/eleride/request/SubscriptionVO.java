package com.eleride.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SubscriptionVO {
    private long id;
    private long userId;
    private long vehicleId;
    private long addressId;
    private boolean isDLVerified;
    private String dlValue;
    private long pricePerMonth;
    private boolean paymentStatus;
    private long paymentId;
    private Timestamp startDate;
    private Timestamp endDate;
}
