package com.eleride.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RegistrationRequest {


	private String password;

	private String name;

	private String email;

	private long mobile;

	private String gender;

	private Boolean isActive;

	private String dlValue;

	private Boolean dlVerified;

	private Boolean isSocialUser;

	private String socialUserType;

}
