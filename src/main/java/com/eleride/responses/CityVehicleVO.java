package com.eleride.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CityVehicleVO {
    private String cityName;
    private int totalVehicleCount;
    private List<VehicleVO> vehiclesList;
}
