package com.eleride.responses;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class VehicleVO {

    private long id;
    private String vehicleName;
    private String vehicleType;
    private String companyName;
    private String modelYear;
    private String mainImage;
    private List<String> allImages;
    private long topSpeed;
    private String features;
    private String description;
    private long pricePerMonth;
}
