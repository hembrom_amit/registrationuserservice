package com.eleride.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PinDetailsResponse {
    private long cityId;
    private String city;
    private List<PinDetailsVO> pinDetailsList;
}
