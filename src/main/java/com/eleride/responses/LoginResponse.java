package com.eleride.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoginResponse {
    private long userId;
    private String email;
    private String sessionId;
    private String name;
    private long mobile;
    private String dlValue;
    private boolean dlVerified;
    private String gender;
    private Boolean isActive;
}
