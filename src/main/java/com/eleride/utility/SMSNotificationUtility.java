package com.eleride.utility;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class SMSNotificationUtility {

    // Find your Account Sid and Token at twilio.com/user/account
    public static final String ACCOUNT_SID = "AC888fa595c310ccaba87f833885f90324";
    //public static final String ACCOUNT_SID = "OR041ace04180eae099aaa7c3c060c1b33";
    public static final String AUTH_TOKEN = "cfb019f9f15bcd27feaf1fa7e32be5dc";

    public SMSNotificationUtility() {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
    }

    public String sendRegistrationOTPSMS(String mobileNumber, String otpValue) {
        String message =  "Your EleRide verification code is "+otpValue +".This otp will expire in 5 minutes.";
        return  sendSMSByTwilio(mobileNumber, message);
    }

    public String sendForgotPasswordSMS(String mobileNumber, String tempPassword) {
        String message = "Your temporary password is "+tempPassword +".This will expire in 5 minutes." ;
        return  sendSMSByTwilio(mobileNumber, message);
    }
    public String sendSMSByTwilio(String mobileNumber,  String body) {
        Message message = Message.creator(new PhoneNumber("+91"+mobileNumber),
                new PhoneNumber("+19036051622"),
                body ).create();
        return  message.getSid();
    }

}
