package com.eleride.monitor;

import com.eleride.utility.SMSNotificationUtility;
import com.google.common.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

@Slf4j
public class MonitorTask implements Runnable {
    private String identifierName;
    private BasicDataGraphite basicDataGraphiteMap;
    private LoadingCache<String, Map<String,BasicDataGraphite>> cacheData;
    private static String CURRENT = "current";
    private static String PREVIOUS = "previous";

    public MonitorTask(LoadingCache<String, Map<String,BasicDataGraphite>> cacheData) {
        this.cacheData = cacheData;
    }

    public MonitorTask(String identifierName, BasicDataGraphite basicDataGraphiteMap) {
        this.identifierName = identifierName;
        this.basicDataGraphiteMap = basicDataGraphiteMap;
    }

    public String getIdentifierName() {
        return identifierName;
    }

    @Override
    public String toString() {
        return "MonitorTask{" +
                "identifierName='" + identifierName + '\'' +
                ", basicDataGraphiteMap=" + basicDataGraphiteMap +
                '}';
    }

    public void run() {
        try {
            System.out.println("Executing monitor task : " + this.toString());
            pushDataToGraphite();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void pushDataToGraphite() throws IOException {
        if(cacheData!=null){
                Socket socket = new Socket("localhost", 2003);
                Writer writer = new OutputStreamWriter(socket.getOutputStream());
                try {
                for(String identifier : this.cacheData.asMap().keySet()){
                    Map<String,BasicDataGraphite> dataGraphiteMap = cacheData.get(identifier);
                    BasicDataGraphite basicDataGraphiteOld = dataGraphiteMap.get(CURRENT);
                    if(basicDataGraphiteOld != null){
                        dataGraphiteMap.put(PREVIOUS,basicDataGraphiteOld);
                        dataGraphiteMap.put(CURRENT,null);

                        Long timestamp = basicDataGraphiteOld.getCurrentTimeInMS();
                        //System.out.println(timestamp);

                        String sentMessage = identifier+".timeTakenInMS"+" "+(int)(basicDataGraphiteOld.getTimeInMS().get()/basicDataGraphiteOld.getCount().get()) +" "+ timestamp+"\n"+
                                identifier+".count"+" "+basicDataGraphiteOld.getCount().get() +" "+ timestamp+"\n"+
                                identifier+".status.2XX"+" "+basicDataGraphiteOld.get__2XX().get() +" "+ timestamp+"\n"+
                                identifier+".status.4XX"+" "+basicDataGraphiteOld.get__4XX().get() +" "+ timestamp+"\n"+
                                identifier+".status.5XX"+" "+basicDataGraphiteOld.get__5XX().get() +" "+ timestamp+"\n";
                        System.out.println(sentMessage);
                        writer.write(sentMessage);
                        writer.flush();
                    }
                }
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

    }
}