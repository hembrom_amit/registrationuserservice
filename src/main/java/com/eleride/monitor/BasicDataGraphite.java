package com.eleride.monitor;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.concurrent.atomic.AtomicInteger;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BasicDataGraphite {
    private long currentTimeInMS;
    private AtomicInteger count;
    private AtomicInteger timeInMS;
    private AtomicInteger __2XX;
    private AtomicInteger __4XX;
    private AtomicInteger __5XX;

    public int incrementCount(){
        return count.incrementAndGet();
    }
    public int incrementTimeInMS(int time){
        return timeInMS.addAndGet(time);
    }
    public int increment2XX(){
        return __2XX.incrementAndGet();
    }
    public int increment4XX(){
        return __4XX.incrementAndGet();
    }
    public int increment5XX(){
        return __5XX.incrementAndGet();
    }

}
