package com.eleride.monitor;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

@Aspect
@Component
@Slf4j
@ConditionalOnExpression("${aspect.enabled:true}")
public class MonitorAppExecution {

    ExecutorService executorService = Executors.newFixedThreadPool(10);

    private LoadingCache<String, Map<String,BasicDataGraphite>> stringMapLoadingCache;

    private static String CURRENT = "current";
    private static String PREVIOUS = "previous";

    ScheduledExecutorService ses = Executors.newScheduledThreadPool(1);


    @PostConstruct
    public void init() {
        int maxSizeObj = 100000;
        int expireTime = 480;
        stringMapLoadingCache = CacheBuilder.newBuilder().maximumSize(maxSizeObj).expireAfterAccess(expireTime, TimeUnit.MINUTES)
                .build(new CacheLoader<String, Map<String,BasicDataGraphite>>() {
                    @Override
                    public Map<String,BasicDataGraphite> load(String sessionid) throws Exception {
                        return getSession(sessionid);
                    }
                });
        MonitorTask monitorTask = new MonitorTask(stringMapLoadingCache);

        ScheduledFuture<?> scheduledFuture = ses.scheduleAtFixedRate(monitorTask, (60-LocalDateTime.now().getSecond()), 60, TimeUnit.SECONDS);
    }
    public LoadingCache<String, Map<String,BasicDataGraphite>> getCache() {
        return stringMapLoadingCache;
    }

    private Map<String,BasicDataGraphite> getSession(String sessionid) {
        return new HashMap<String,BasicDataGraphite>();
    }

    @Around("@annotation(com.eleride.monitor.MonitorApp)")
    public Object executionTime(ProceedingJoinPoint point) throws Throwable {
        long startTime = System.currentTimeMillis();
        Object object = point.proceed();
        long endtime = System.currentTimeMillis();
        int timeTaken = (int)(endtime-startTime);
        int status = 200;
        ResponseEntity responseEntity = (ResponseEntity)object;
        status = responseEntity.getStatusCodeValue();
        String identifier = point.getSignature().getDeclaringTypeName()+"."+point.getSignature().getName();
        Map<String,BasicDataGraphite> basicDataGraphiteMap = getCache().get(identifier);
        basicDataGraphiteMap = updateCache(basicDataGraphiteMap, timeTaken, status, identifier);
        getCache().put(identifier, basicDataGraphiteMap);
        return object;
    }

    private Map<String, BasicDataGraphite> updateCache(Map<String, BasicDataGraphite> basicDataGraphiteMap, int timeTaken, int status, String identifier) {

        if(basicDataGraphiteMap == null || basicDataGraphiteMap.isEmpty()){
            AtomicInteger __2XX = new AtomicInteger(0);
            AtomicInteger __4XX = new AtomicInteger(0);
            AtomicInteger __5XX = new AtomicInteger(0);
            if(status>=200 && status<300){
                __2XX.incrementAndGet();
            } else if(status>=400&& status<500){
                __4XX.incrementAndGet();
            } else {
                __5XX.incrementAndGet();
            }
            BasicDataGraphite basicDataGraphite = BasicDataGraphite.builder().currentTimeInMS(System.currentTimeMillis() / 1000).count(new AtomicInteger(1)).timeInMS(new AtomicInteger(timeTaken)).__2XX(__2XX).__4XX(__4XX).__5XX(__5XX).build();
            basicDataGraphiteMap.put(CURRENT,basicDataGraphite);
            getCache().put(identifier, basicDataGraphiteMap);
        } else{
            if(basicDataGraphiteMap.get(CURRENT) !=null) {
                BasicDataGraphite basicDataGraphite = basicDataGraphiteMap.get(CURRENT);
                basicDataGraphite.incrementCount();
                basicDataGraphite.incrementTimeInMS(timeTaken);
                if(status>=200 && status<300){
                    basicDataGraphite.increment2XX();
                } else if(status>=400&& status<500){
                    basicDataGraphite.increment4XX();
                } else {
                    basicDataGraphite.increment5XX();
                }
            } else {
                BasicDataGraphite basicDataGraphiteOld = basicDataGraphiteMap.get(CURRENT);
                basicDataGraphiteMap.put(PREVIOUS,basicDataGraphiteOld);
                AtomicInteger __2XX = new AtomicInteger(0);
                AtomicInteger __4XX = new AtomicInteger(0);
                AtomicInteger __5XX = new AtomicInteger(0);
                if(status>=200 && status<300){
                    __2XX.incrementAndGet();
                } else if(status>=400&& status<500){
                    __4XX.incrementAndGet();
                } else {
                    __5XX.incrementAndGet();
                }
                BasicDataGraphite basicDataGraphite = BasicDataGraphite.builder().currentTimeInMS(System.currentTimeMillis() / 1000).count(new AtomicInteger(1)).timeInMS(new AtomicInteger(timeTaken)).__2XX(__2XX).__4XX(__4XX).__5XX(__5XX).build();
                basicDataGraphiteMap.put(CURRENT,basicDataGraphite);
            }
        }
        getCache().put(identifier, basicDataGraphiteMap);
        return basicDataGraphiteMap;
    }


}